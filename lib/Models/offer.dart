class Offer {
  String? title;
  String? id;
  String? dateRange;
  String? isActive;
  String? details;
  String? categoryType;
  String? categoryValue;

  Offer(
      {required this.title,
      required this.id,
      required this.dateRange,
      required this.isActive,
      required this.details,
      required this.categoryType,
      required this.categoryValue});

  Offer.fromJson(Map json) {
    title = json['title'];
    dateRange = json['date_range'];
    id = json['id'];
    isActive = json['is_active'];
    details = json['details'];
    categoryType = json['category_type'];
    categoryValue = json['category_value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['title'] = title;
    data['id'] = id;
    data['date_range'] = dateRange;
    data['is_active'] = isActive;
    data['details'] = details;
    data['category_type'] = categoryType;
    data['category_value'] = categoryValue;

    return data;
  }
}
