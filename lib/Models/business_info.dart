class BusinessInfo {
  // String? businessCategory;
  // String? businessReviewLink;
  String? lastName;

  // String? businessLocationLink;
  String? mobileNumber;
  String? firstName;

  // String? businessContact;
  // String? businessTitle;
  String? googleBusinessId;
  String? googleBusinessName;
  String? googleBusinessCoordinates;
  String? rating;
  List<String>? types;
  int? totalOffersViewed;
  int? totalOffersClaimed;

  BusinessInfo(
      {required this.lastName,
      required this.mobileNumber,
      required this.firstName,
      required this.googleBusinessId,
      required this.googleBusinessName,
      required this.googleBusinessCoordinates,
      required this.rating,
      required this.types,
      this.totalOffersViewed,
      this.totalOffersClaimed});

  BusinessInfo.fromJson(Map json) {
    lastName = json['last_name'];
    mobileNumber = json['mobile_number'];
    firstName = json['first_name'];
    googleBusinessId = json['google_business_id'];
    googleBusinessName = json['google_business_name'];
    rating = json['rating'];
    types = json['types'] == null ? [] : json['types'].toList().cast<String>();
    googleBusinessCoordinates = json['google_business_coordinates'];
    totalOffersViewed = json['total_offers_viewed'] ?? 0;
    totalOffersClaimed = json['total_offers_claimed'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['last_name'] = lastName;
    data['mobile_number'] = mobileNumber;
    data['first_name'] = firstName;
    data['google_business_id'] = googleBusinessId;
    data['google_business_name'] = googleBusinessName;
    data['rating'] = rating;
    data['types'] = types;
    data['google_business_coordinates'] = googleBusinessCoordinates;
    return data;
  }
}
