
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import '../../Controllers/business_info_controller.dart';
import '../../Controllers/offer_controller.dart';
import '../../Models/offer.dart';
import '../../Utils/app_colors.dart';
import '../../Utils/route_names.dart';
import '../../Widgets/custom_button.dart';
import '../../Widgets/custom_text_field.dart';
import '../../Widgets/profile_header.dart';

class EditOfferScreen extends StatefulWidget {
  final int? offerIndex;

  const EditOfferScreen({Key? key, this.offerIndex}) : super(key: key);

  @override
  State<EditOfferScreen> createState() => _EditOfferScreenState();
}

class _EditOfferScreenState extends State<EditOfferScreen> {
  final _dateRangeController = TextEditingController();
  final _offerDetailsController = TextEditingController();
  final _categoryController = TextEditingController();
  final _getController = TextEditingController();
  final RxBool _isActive = true.obs;

  final offerController = Get.find<OfferController>();
  final BusinessInfoController businessInfoController =
      Get.find<BusinessInfoController>();

  final _categoryList = ["Discount", "CashBack", "Buy & Get"];

  late final RxString _selectedCategory;

  void _selectRange() async {
    final selectedRange = await showDateRangePicker(
        context: context, firstDate: DateTime.now(), lastDate: DateTime(2050));

    if (selectedRange != null) {
      String startDate = selectedRange.start.toString().split(" ")[0];
      String endDate = selectedRange.end.toString().split(" ")[0];
      _dateRangeController.text = "$startDate to $endDate";
    }
    setState(() {});
  }

  void _saveOffer() {
    bool isBuyGet = _selectedCategory.value == _categoryList.last;
    if (_dateRangeController.text.isEmpty ||
        _categoryController.text.isEmpty ||
        (isBuyGet ? _getController.text.isEmpty : false) ||
        _offerDetailsController.text.isEmpty) {
      Get.snackbar("Oops!", "Any of the field cannot be left empty");
      return;
    }

    String id = "";

    if (widget.offerIndex == null) {
      id = DateTime.now().toString().replaceAll("-", "");
      String businessId =
          businessInfoController.businessInfo?.googleBusinessId ?? "asfhasdfhc";
      id = id.replaceAll(" ", "");
      id = id.replaceAll(":", "");
      id = id.replaceAll(".", "");
      id =
          "${businessId.substring(0, 3)}$id${businessId.substring(businessId.length - 3, businessId.length)}";
      final shuffledId = id.split('');
      shuffledId.shuffle();
      id = shuffledId.join('');
    } else {
      id = offerController.offerList[widget.offerIndex ?? 0].id ?? "";
    }

    String title = _selectedCategory.value == _categoryList.first
        ? '${_categoryController.text}% Off'
        : isBuyGet
            ? 'Buy ${_categoryController.text} & Get ${_getController.text}'
            : 'Flat ${_categoryController.text}% Cashback';

    Offer offer = Offer(
        title: title,
        id: id,
        dateRange: _dateRangeController.text,
        details: _offerDetailsController.text,
        isActive: _isActive.value.toString(),
        categoryType: _selectedCategory.value,
        categoryValue:
            'category=${_categoryController.text} get=${_getController.text}');

    offerController.changeOfferList(offer, widget.offerIndex).then((value) {
      if (value) {
        Get.offAllNamed(RouteNames.BOTTOM_BAR_SCREEN);
        Get.snackbar("Hurray",
            "Offer ${widget.offerIndex == null ? "Created" : "Edited"} Successfully");
      }
    });
  }

  void _add(TextEditingController controller) {
    bool isBuyGet = _selectedCategory.value == _categoryList.last;
    int value = controller.text.isEmpty ? 0 : int.parse(controller.text);
    value += isBuyGet ? 1 : 5;

    if (value > (isBuyGet ? 5 : 100)) {
      return;
    }
    controller.text = value.toString();
    if (value == 5) setState(() {});
  }

  void _subtract(TextEditingController controller) {
    bool isBuyGet = _selectedCategory.value == _categoryList.last;
    int value = controller.text.isEmpty ? 0 : int.parse(controller.text);
    value -= isBuyGet ? 1 : 5;

    if (value <= 0) {
      return;
    }
    controller.text = value.toString();
  }

  @override
  void initState() {
    super.initState();
    if (widget.offerIndex != null) {
      Offer offer = offerController.offerList[widget.offerIndex ?? 0];

      _dateRangeController.text = offer.dateRange ?? "";
      _offerDetailsController.text = offer.details ?? "";
      _selectedCategory = (offer.categoryType ?? _categoryList[0]).obs;
      _isActive.value = offer.isActive == "true";

      final categoryValue = (offer.categoryValue ?? "")
          .replaceAll('category=', '')
          .replaceAll('get=', '')
          .split(' ');

      _categoryController.text = categoryValue[0];
      _getController.text = categoryValue[1];
    } else {
      _selectedCategory = _categoryList[0].obs;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 30, top: 20, bottom: 50),
                child: ProfileHeader(
                  title:
                      "${widget.offerIndex == null ? "Create" : "Edit"} Offer",
                  onBack: () => Get.back(),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 15, bottom: 15),
                alignment: Alignment.centerLeft,
                child: const Text(
                  "Select Category",
                  style: TextStyle(
                      color: AppColors.darkText,
                      fontSize: 15,
                      fontWeight: FontWeight.w700),
                ),
              ),
              _buildCustomDropDown(),
              Obx(() => _buildCustomField(
                  _selectedCategory.value == _categoryList.last ? "Buy" : '10%',
                  _categoryController)),
              Obx(() => _selectedCategory.value == _categoryList.last
                  ? _buildCustomField('Get', _getController)
                  : const SizedBox()),
              const SizedBox(height: 20),
              CustomTextField(
                hintText: "Date Range",
                textEditingController: _dateRangeController,
                onTap: _selectRange,
              ),
              const SizedBox(height: 20),
              CustomTextField(
                hintText: "Offer Details",
                maxLines: 5,
                textEditingController: _offerDetailsController,
              ),
              const SizedBox(height: 15),
              Row(
                children: [
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Text(
                      "Active",
                      style: TextStyle(color: AppColors.darkText, fontSize: 17),
                    ),
                  ),
                  Obx(() => Checkbox(
                      value: _isActive.value,
                      onChanged: (value) => _isActive.value = value as bool))
                ],
              ),
              const SizedBox(height: 15),
              CustomButton(label: "Save", onTap: _saveOffer)
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildCustomField(String hintText, TextEditingController controller) {
    return Padding(
      padding: const EdgeInsets.only(left: 15, top: 20),
      child: Row(
        children: [
          _buildCustomButton(Icons.add, () => _add(controller)),
          Stack(
            children: [
              Container(
                  margin: const EdgeInsets.symmetric(horizontal: 7),
                  width: MediaQuery.of(context).size.width * 0.3,
                  child: CustomTextField(
                    hintText: hintText,
                    textEditingController: controller,
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp(r"\d")),
                      TextInputFormatter.withFunction((oldValue, newValue) {
                        final text = newValue.text;
                        return text.isEmpty
                            ? newValue
                            : int.tryParse(text) == null ||
                                    int.parse(text) > 100
                                ? oldValue
                                : newValue;
                      }),
                    ],
                  )),
              if (controller.text.isNotEmpty)
                Positioned(
                  top: 18,
                  right: 40,
                  child: Obx(() => _selectedCategory.value == _categoryList.last
                      ? const SizedBox()
                      : const Text("(%)",
                          style: TextStyle(
                              color: AppColors.darkText, fontSize: 15))),
                )
            ],
          ),
          _buildCustomButton(Icons.remove, () => _subtract(controller)),
        ],
      ),
    );
  }

  Widget _buildCustomButton(IconData icon, [void Function()? onTap]) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
            color: AppColors.lightBlue,
            borderRadius: BorderRadius.circular(15)),
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        child: Icon(icon, color: AppColors.background, size: 30),
      ),
    );
  }

  Widget _buildCustomDropDown() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
          color: AppColors.background,
          borderRadius: BorderRadius.circular(22),
          boxShadow: const [
            BoxShadow(
                color: Color.fromRGBO(90, 108, 234, 0.07),
                offset: Offset(0, 0),
                spreadRadius: 10,
                blurRadius: 50)
          ]),
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      alignment: Alignment.center,
      child: Obx(() => DropdownButton(
            value: _selectedCategory.value,
            isExpanded: true,
            underline: const SizedBox(),
            items: _categoryList
                .map((e) => DropdownMenuItem(value: e, child: Text(e)))
                .toList(),
            onChanged: (value) {
              _selectedCategory.value = value.toString();
              _categoryController.clear();
              _getController.clear();
            },
          )),
    );
  }
}
