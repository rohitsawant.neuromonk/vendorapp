import 'dart:async';

import 'package:flutter/material.dart';
import 'package:vendorapp/Screens/Onboarding/welcome_screen.dart';
import 'package:vendorapp/main.dart';

import '../../Utils/app_colors.dart';
import '../../Utils/app_images.dart';


class ScreenSplash extends StatefulWidget {
  const ScreenSplash({super.key});

  @override
  State<ScreenSplash> createState() => _ScreenSplashState();
}

class _ScreenSplashState extends State<ScreenSplash> {


  @override
  Widget build(BuildContext context) {
  //  Timer(Duration(seconds: 5), ()=>getInitialRoute());
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 180),
          child: Column(
            children: [
              const SizedBox(height: 100),
              Align(
                  alignment: Alignment.center,
                  child: Image.asset(
                    AppImages.knoffslogo,
                  )),
              // Image.asset("assets/images/logo1.png"),
              const Text(
                "Know nearby offers",
                style: TextStyle(
                    color: AppColors.darkGrey,
                    fontSize: 18,
                    fontWeight: FontWeight.w400),
              ),
              // Image.asset('assets/images/welcome.png',height: 300,width: 300,),
              // Align(
              //     alignment: Alignment.center,
              //     child: Image.asset(AppImages.rateIllustration, height: 300)),
              // const SizedBox(height: 24),
              // RichText(
              //     text: const TextSpan(
              //         text: "K N O F F S",
              //         style: TextStyle(
              //             fontSize: 50,
              //             height: 1.5,
              //             fontWeight: FontWeight.w300,
              //             color: AppColors.darkText),
              //         children: [
              //       // TextSpan(
              //       //     text: "Reviewer",
              //       //     style: TextStyle(
              //       //         color: AppColors.darkText, fontSize: 36, height: 1.5))
              //     ])),
              // const Text(
              //   "Know nearby offers",
              //   style: TextStyle(
              //       color: AppColors.darkGrey,
              //       fontSize: 18,
              //       fontWeight: FontWeight.w400),
              // ),
              // const Expanded(child: SizedBox()),
              // InkWell(
              //   onTap: (){},
              //   child: Container(
              //     width: 228,
              //     padding: const EdgeInsets.symmetric(vertical: 24),
              //     decoration: BoxDecoration(
              //         color: AppColors.background,
              //         border: Border.all(color: AppColors.darkText),
              //         borderRadius: BorderRadius.circular(15),
              //         boxShadow: const [
              //           BoxShadow(
              //               color: Color.fromRGBO(90, 108, 234, 0.150),
              //               offset: Offset(0, 26),
              //               blurRadius: 35)
              //         ]),
              //     alignment: Alignment.center,
              //     child: Row(
              //       mainAxisAlignment: MainAxisAlignment.center,
              //       children: [
              //         Image.asset(AppImages.googleIcon, height: 37),
              //         const SizedBox(width: 18),
              //         const Text(
              //           "Google",
              //           style: TextStyle(
              //               color: AppColors.darkText,
              //               fontSize: 20,
              //               fontWeight: FontWeight.w400),
              //         )
              //       ],
              //     ),
              //   ),
              // ),
              // const SizedBox(height: 22),
              // const Text("Sign Up / Login",
              //     style: TextStyle(
              //         color: AppColors.darkText,
              //         fontSize: 16,
              //         fontWeight: FontWeight.w700)),
              // const SizedBox(height: 46),
            ],
          ),
        ),
      ),
      // floatingActionButton: FloatingActionButton(onPressed: (() => Rout())),
    );
    
  }
}
