
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shimmer/shimmer.dart';
import 'package:vendorapp/Controllers/offer_controller.dart';
import '../../Controllers/business_info_controller.dart';
import '../../Models/offer.dart';
import '../../Utils/app_colors.dart';
import '../../Utils/app_constants.dart';
import '../../Utils/app_images.dart';
import '../../Utils/route_names.dart';
import '../../Widgets/custom_search_bar.dart';
import '../../Widgets/offer_box.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final offerController = Get.put(OfferController());
  final RxList<Offer> _filteredOfferList = <Offer>[].obs;
  final BusinessInfoController businessInfoController =
      Get.find<BusinessInfoController>();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          const SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 18),
            child: _buildHeader(),
          ),
          _buildSearchBar(),
          const SizedBox(height: 15),
          FutureBuilder(
            future: offerController.getOffers(),
            builder: (context, snapshot) {
              if (snapshot.data == true) {
                _filteredOfferList.value = offerController.offerList;
              }
              debugPrint(
                  "Connection State ----> ${snapshot.connectionState} || ${offerController.isLoading.value}");
              return snapshot.connectionState == ConnectionState.done
                  ? snapshot.data == false
                      ? Container(
                          height: MediaQuery.of(context).size.height * .6,
                          alignment: Alignment.center,
                          child: const Text(
                            "No Offers Available!",
                            style: TextStyle(
                                color: AppColors.darkText,
                                fontWeight: FontWeight.w600),
                          ),
                        )
                      : Obx(() => offerController.isLoading.value
                          ? _buildShimmerContainers()
                          : _filteredOfferList.isEmpty
                              ? Container(
                                  height:
                                      MediaQuery.of(context).size.height * .6,
                                  alignment: Alignment.center,
                                  child: const Text(
                                    "No Offers Found!",
                                    style: TextStyle(
                                        color: AppColors.darkText,
                                        fontWeight: FontWeight.w600),
                                  ),
                                )
                              : SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height * .6,
                                  child: ListView.builder(
                                    itemCount: _filteredOfferList.length,
                                    itemBuilder: (context, index) => OfferBox(
                                      offer: _filteredOfferList[index],
                                      image: AppImages.food,
                                      onTap: () => Get.toNamed(
                                          RouteNames.EDIT_OFFER_SCREEN,
                                          arguments: index),
                                      onDelete: () async {
                                        await offerController
                                            .deleteOffer(index);
                                        setState(() {});
                                      },
                                    ),
                                  ),
                                ))
                  : _buildShimmerContainers();
            },
          ),
        ],
      ),
    );
  }

  Widget _buildShimmerContainers() {
    return Shimmer.fromColors(
      baseColor: Colors.grey[300]!,
      highlightColor: Colors.grey[100]!,
      child: ListView.builder(
        itemCount: 4,
        shrinkWrap: true,
        itemBuilder: (context, index) => Container(
          height: 95,
          margin: const EdgeInsets.symmetric(horizontal: 14, vertical: 10),
          decoration: BoxDecoration(
              color: AppColors.background,
              borderRadius: BorderRadius.circular(10)),
        ),
      ),
    );
  }

  Widget _buildHeader() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              "Welcome Back,",
              style: TextStyle(
                  color: AppColors.darkGrey,
                  fontFamily: AppConstants.poppinsFonts,
                  fontWeight: FontWeight.w400,
                  fontSize: 12),
            ),
            const SizedBox(height: 5),
            Obx(() => Text(
                  businessInfoController.userName.value,
                  style: const TextStyle(
                      color: AppColors.darkText,
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                      fontFamily: AppConstants.poppinsFonts),
                ))
          ],
        ),
        GestureDetector(
          onTap: () => Get.toNamed(RouteNames.NOTIFICATION_SCREEN),
          child: Container(
            height: 40,
            width: 40,
            decoration: BoxDecoration(
              color: AppColors.lightGrey.withOpacity(.5),
              borderRadius: BorderRadius.circular(8),
            ),
            alignment: Alignment.center,
            child: Image.asset(
              AppImages.notificationIcon,
              height: 18,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildSearchBar() {
    return Row(
      children: [
        const SizedBox(width: 16),
        Expanded( 
          child: CustomSearchBar(
            hintText: "Search Offers",
            onSearch: (keyword) {
              _filteredOfferList.value = [];
              if (keyword.isEmpty) {
                _filteredOfferList.value = offerController.offerList;
                return;
              }

              for (Offer offer in offerController.offerList) {
                if ((offer.title ?? "")
                    .toLowerCase()
                    .contains(keyword.toLowerCase())) {
                  _filteredOfferList.add(offer);
                }
              }
            },
          ),
        ),
        const SizedBox(width: 16),

        // GestureDetector(
        //   onTap: () => Get.toNamed(RouteNames.FILTER_SCREEN),
        //   child: Container(
        //     height: 50,
        //     width: 50,
        //     margin: const EdgeInsets.only(right: 31),
        //     decoration: BoxDecoration(
        //         color: AppColors.lightOrange.withOpacity(.15),
        //         borderRadius: BorderRadius.circular(15)),
        //     alignment: Alignment.center,
        //     child: Image.asset(AppImages.filterIcon, height: 20),
        //   ),
        // ),
      ],
    );
  }
}
