import 'dart:convert';
import 'dart:developer';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:get/get.dart';

import '../../Controllers/business_info_controller.dart';
import '../../Utils/app_colors.dart';
import '../../Utils/app_images.dart';
import '../../Utils/route_names.dart';
import '../Home/home_screen.dart';
import '../Profile/profile_screen.dart';

class BottomBarScreen extends StatefulWidget {
  const BottomBarScreen({Key? key}) : super(key: key);

  @override
  State<BottomBarScreen> createState() => _BottomBarScreenState();
}

class _BottomBarScreenState extends State<BottomBarScreen> {
  int _currentIndex = 0;

  late final List _screens;
  final BusinessInfoController businessInfoController =
      Get.put(BusinessInfoController());

  @override
  void initState() {
    super.initState();
    _init();
    _screens = [
      const HomeScreen(),
      const HomeScreen(),
      ProfileScreen(
        onBack: () {
          _currentIndex = 0;
          setState(() {});
        },
      )
    ];
  }

  void _init() {
    if (businessInfoController.businessInfo == null) {
      businessInfoController.getBusinessInfo().then((value) {
        if (value && businessInfoController.businessInfo != null) {
          businessInfoController.userName.value =
              "${businessInfoController.businessInfo?.firstName ?? "Sir"} ${businessInfoController.businessInfo?.lastName ?? ""}";
          setState(() {});
        }
      });
    } else {
      businessInfoController.userName.value =
          "${businessInfoController.businessInfo?.firstName ?? "Sir"} ${businessInfoController.businessInfo?.lastName ?? ""}";
    }
  }

  void _updateOfferStatus(String userId, String offerStatus) {
    FirebaseFirestore.instance.collection("users").doc(userId).set({
      "info": {"recent_offer_status": offerStatus}
    }, SetOptions(merge: true));
  }

  Future _scanAndVerifyQR() async {
    try {
      String barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          "#00e676", "Cancel", true, ScanMode.QR);
      final qrDetails = jsonDecode(barcodeScanRes);
      log(barcodeScanRes, name: 'Qr Scan Result');
      if (barcodeScanRes != "-1" &&
          qrDetails["business_id"] ==
              businessInfoController.businessInfo?.googleBusinessId) {
        // Get.toNamed(RouteNames.SUCCESS_SCREEN, arguments: true);
        Get.dialog(CupertinoAlertDialog(
          title: const Text("Success"),
          content: Text(
              "We have successfully verified QR Code of the offer titled '${qrDetails["offer_title"]}'. Claimed by ${qrDetails["user_name"]}"),
          actions: [
            TextButton(onPressed: () => Get.back(), child: const Text("Ok"))
          ],
        ));
        businessInfoController.updateOffersClaimed();
        _updateOfferStatus(qrDetails["user_id"], "valid");
      } else {
        _updateOfferStatus(qrDetails["user_id"], "invalid");
        throw "Business id didn't matched";
      }
      return;
    } catch (e) {
      Get.dialog(CupertinoAlertDialog(
        title: const Text("Error"),
        content: const Text("This QR Code is invalid"),
        actions: [
          TextButton(onPressed: () => Get.back(), child: const Text("Ok"))
        ],
      ));
      debugPrint("Error while scanning qr: $e");
      return;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: _buildBottomNavigationBar(),
      floatingActionButton: _currentIndex == 0
          ? _buildOfferButton(() => Get.toNamed(RouteNames.EDIT_OFFER_SCREEN))
          : null,
      body: SafeArea(child: _screens[_currentIndex]),
    );
  }

  Widget _buildBottomNavigationBar() {
    return Container(
      height: 90,
      decoration: const BoxDecoration(color: AppColors.background, boxShadow: [
        BoxShadow(
            color: Color.fromRGBO(29, 22, 23, 0.20),
            offset: Offset(0, 10),
            blurRadius: 40)
      ]),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          _buildBottomBarItem(
              _currentIndex == 0
                  ? AppImages.homeActive
                  : AppImages.homeInActive,
              0),
          _buildBottomBarItem(AppImages.cameraIcon, 1),
          _buildBottomBarItem(
              _currentIndex == 2
                  ? AppImages.userActive
                  : AppImages.userInActive,
              2)
        ],
      ),
    );
  }

  Widget _buildOfferButton(void Function() onTap) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
        decoration: BoxDecoration(
            color: AppColors.green, borderRadius: BorderRadius.circular(50)),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(
              Icons.add,
              color: AppColors.background,
              size: 25,
            ),
            SizedBox(width: 2.5),
            Text(
              "Add Offer",
              style: TextStyle(
                  color: AppColors.background,
                  fontSize: 15,
                  fontWeight: FontWeight.w700),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildBottomBarItem(String image, int index) {
    return InkWell(
      onTap: () async {
        if (index == 1) {
          _scanAndVerifyQR();
          return;
        }

        if (_currentIndex != index) {
          setState(() {
            _currentIndex = index;
          });
        }
      },
      child: Container(
          height: 50,
          width: 50,
          alignment: Alignment.center,
          child: Image.asset(
            image,
            width: 30,
          )),
    );
  }
}
