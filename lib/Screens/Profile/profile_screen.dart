
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:url_launcher/url_launcher_string.dart';

import '../../Controllers/business_info_controller.dart';
import '../../Utils/app_images.dart';
import '../../Utils/route_names.dart';
import '../../Utils/shared_prefs.dart';
import '../../Widgets/account_delete_dialog.dart';
import '../../Widgets/action_box.dart';
import '../../Widgets/action_tile.dart';
import '../../Widgets/profile_detail_box.dart';
import '../../Widgets/profile_header.dart';

class ProfileScreen extends StatefulWidget {
  final void Function()? onBack;

  const ProfileScreen({Key? key, this.onBack}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final infoController = Get.find<BusinessInfoController>();
  bool isSigningOut = false;

  Future<void> _deleteAccount() async {
    bool cancel = false;

    await Get.dialog(
        WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
            title: const Text("Are you sure want to delete account?"),
            actions: [
              TextButton(
                  onPressed: () {
                    cancel = true;
                    Get.back();
                  },
                  child: const Text("No")),
              TextButton(onPressed: () => Get.back(), child: const Text("Yes")),
            ],
          ),
        ),
        barrierDismissible: false);
    if (cancel) return;

    Get.dialog(const AccountDeleteDialog(), barrierDismissible: false);
    await infoController.deleteBusiness();
    await SharedPrefs.prefs.clear();
    Get.back();
    infoController.dispose();
    Get.offAllNamed(RouteNames.WELCOME_SCREEN);
  }

  Future<void> _signOut() async {
    if (isSigningOut) {
      Get.snackbar("Error", "Please wait while we're signing you out!");

      return;
    }
    isSigningOut = true;
    await GoogleSignIn().signOut();
    await FirebaseAuth.instance.signOut();
    await SharedPrefs.prefs.clear();
    Get.offAllNamed(RouteNames.WELCOME_SCREEN);
  }

  void _editProfile() {
    if (isSigningOut) {
      Get.snackbar("Error", "Please wait while we're signing you out!");
      return;
    }
    Get.toNamed(RouteNames.CREATE_ACCOUNT_SCREEN);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Column(
        children: [
          const SizedBox(height: 20),
          ProfileHeader(title: "Profile", onBack: widget.onBack),
          const SizedBox(height: 35),
          ProfileDetailBox(
            userName: infoController.userName.value,
            offersViewed: (infoController.businessInfo?.totalOffersViewed ?? 0)
                .toString(),
            offersClaimed:
                (infoController.businessInfo?.totalOffersClaimed ?? 0)
                    .toString(),
            onEdit: _editProfile,
          ),
          const SizedBox(height: 30),
          const ActionBox(actionTiles: [
            // ActionTile(
            //   title: "Personal Data",
            //   img: AppImages.personOutlinedIcon,
            //   onTap: () => Get.toNamed(RouteNames.CREATE_ACCOUNT_SCREEN),
            // ),
            ActionTile(img: AppImages.chartIcon, title: "Activity History")
          ], label: "Account"),
          const SizedBox(height: 30),
          ActionBox(actionTiles: [
            ActionTile(
              img: AppImages.mailIcon,
              title: "Contact Us",
              onTap: () => launchUrlString("mailto:info@breviewer.in"),
            ),
            ActionTile(
              img: AppImages.privacyPolicyIcon,
              title: "Privacy Policy",
              onTap: () => Get.toNamed(RouteNames.PRIVACY_POLICY_SCREEN),
            ),
            ActionTile(
              img: AppImages.personOutlinedIcon,
              title: "About Us",
              onTap: () => Get.toNamed(RouteNames.ABOUT_US_SCREEN),
            ),
            const ActionTile(img: AppImages.settingIcon, title: "Settings"),
            ActionTile(
              img: AppImages.personOutlinedIcon,
              title: "Logout",
              onTap: _signOut,
            ),
            ActionTile(
              img: AppImages.personOutlinedIcon,
              title: "Delete Account",
              onTap: _deleteAccount,
            ),
          ], label: "Other")
        ],
      ),
    );
  }
}
