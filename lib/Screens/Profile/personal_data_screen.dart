import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../Utils/app_images.dart';
import '../../Widgets/action_box.dart';
import '../../Widgets/action_tile.dart';
import '../../Widgets/profile_header.dart';

class PersonalDataScreen extends StatefulWidget {
  const PersonalDataScreen({Key? key}) : super(key: key);

  @override
  State<PersonalDataScreen> createState() => _PersonalDataScreenState();
}

class _PersonalDataScreenState extends State<PersonalDataScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30),
          child: Column(
            children: [
              const SizedBox(height: 20),
              ProfileHeader(
                title: "Personal Data",
                onBack: () => Get.back(),
              ),
              const SizedBox(height: 70),
              const ActionBox(actionTiles: [
                ActionTile(img: AppImages.settingIcon, title: "First Name"),
                ActionTile(img: AppImages.settingIcon, title: "Last Name"),
                ActionTile(img: AppImages.mailIcon, title: "Address"),
                ActionTile(img: AppImages.chartIcon, title: "Phone"),
              ], label: "Personal")
            ],
          ),
        ),
      ),
    );
  }
}
