
import 'package:flutter/material.dart';

import '../../Utils/app_constants.dart';

class AboutUs extends StatelessWidget {
  const AboutUs({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
          child: Text(AppConstants.aboutUs),
        ),
      ),
    );
  }
}
