
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../Utils/app_colors.dart';
import '../../Utils/app_images.dart';
import '../../Utils/route_names.dart';

class SuccessScreen extends StatefulWidget {
  final bool isQRSuccess;
  const SuccessScreen({Key? key, this.isQRSuccess = false}) : super(key: key);

  @override
  State<SuccessScreen> createState() => _SuccessScreenState();
}

class _SuccessScreenState extends State<SuccessScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            const Expanded(flex: 4, child: SizedBox()),
            Image.asset(AppImages.success, height: 162),
            const SizedBox(height: 33),
            const Text(
              "Congrats!",
              style: TextStyle(
                  color: AppColors.lightGreen,
                  fontSize: 30,
                  fontWeight: FontWeight.w700),
            ),
            const SizedBox(
              height: 12,
            ),
            Text(
              widget.isQRSuccess
                  ? "QR Scanned Successfully"
                  : "Your Profile Is Ready To Use",
              style: const TextStyle(
                  color: AppColors.darkText,
                  fontSize: 23,
                  fontWeight: FontWeight.w700),
            ),
            const Expanded(flex: 3, child: SizedBox()),
            // const Expanded(child: SizedBox()),
            Align(
                alignment: const Alignment(.85, 0),
                child: InkWell(
                    onTap: () => Get.offAllNamed(RouteNames.ONBOARDING_SCREEN),
                    child: Image.asset(AppImages.fullProgress, height: 60))),
            const SizedBox(height: 40),
          ],
        ),
      ),
    );
  }
}
