
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';

import '../../Controllers/auth_controller.dart';
import '../../Controllers/business_info_controller.dart';
import '../../Utils/app_colors.dart';
import '../../Utils/app_constants.dart';
import '../../Utils/app_images.dart';
import '../../Utils/route_names.dart';
import '../../Utils/shared_prefs.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  State<WelcomeScreen> createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  GestureRecognizer? gestureRecognizer;

  @override
  void initState() {
    super.initState();
    // getLocationPermission();
    gestureRecognizer = TapGestureRecognizer()
      ..onTap = () {
        Get.toNamed(RouteNames.CREATE_ACCOUNT_SCREEN);
      };
  }

  Future _signInWithGoogle() async {
    bool isSuccess = await AuthController().signInWithGoogle();

    if (!isSuccess) {
      return;
    }

    bool isOldUser = await BusinessInfoController().getBusinessInfo();

    if (isOldUser) {
      SharedPrefs.prefs.setString(
          SharedPrefsKeys.userStatus, AppConstants.readyToUseAppStatus);
      Get.toNamed(RouteNames.BOTTOM_BAR_SCREEN);
    } else {
      SharedPrefs.prefs.setString(
          SharedPrefsKeys.userStatus, AppConstants.profileCreationLeftStatus);
      Get.toNamed(RouteNames.CREATE_ACCOUNT_SCREEN);
    }
  }

  // void getLocationPermission() async {
  //   LocationPermission permission = await Geolocator.checkPermission();
  //   if (permission == LocationPermission.denied) {
  //     permission = await Geolocator.requestPermission();
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            const SizedBox(height: 100),
             Align(
                alignment: Alignment.center,
                child: Image.asset(AppImages.knoffslogo,)),
            // Image.asset("assets/images/logo1.png"),
             const Text(
              "Know nearby offers",
              style: TextStyle(
                  color: AppColors.darkGrey,
                  fontSize: 18,
                  fontWeight: FontWeight.w400),
            ),
            // Image.asset('assets/images/welcome.png',height: 300,width: 300,),
            Align(
                alignment: Alignment.center,
                child: Image.asset(AppImages.rateIllustration, height: 300)),
            const SizedBox(height: 24),
            // RichText(
            //     text: const TextSpan(
            //         text: "K N O F F S",
            //         style: TextStyle(
            //             fontSize: 50,
            //             height: 1.5,
            //             fontWeight: FontWeight.w300,
            //             color: AppColors.darkText),
            //         children: [
            //       // TextSpan(
            //       //     text: "Reviewer",
            //       //     style: TextStyle(
            //       //         color: AppColors.darkText, fontSize: 36, height: 1.5))
            //     ])),
            // const Text(
            //   "Know nearby offers",
            //   style: TextStyle(
            //       color: AppColors.darkGrey,
            //       fontSize: 18,
            //       fontWeight: FontWeight.w400),
            // ),
            const Expanded(child: SizedBox()),
            InkWell(
              onTap: _signInWithGoogle,
              child: Container(
                width: 228,
                padding: const EdgeInsets.symmetric(vertical: 24),
                decoration: BoxDecoration(
                    color: AppColors.background,
                    border: Border.all(color: AppColors.darkText),
                    borderRadius: BorderRadius.circular(15),
                    boxShadow: const [
                      BoxShadow(
                          color: Color.fromRGBO(90, 108, 234, 0.150),
                          offset: Offset(0, 26),
                          blurRadius: 35)
                    ]),
                alignment: Alignment.center,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(AppImages.googleIcon, height: 37),
                    const SizedBox(width: 18),
                    const Text(
                      "Google",
                      style: TextStyle(
                          color: AppColors.darkText,
                          fontSize: 20,
                          fontWeight: FontWeight.w400),
                    )
                  ],
                ),
              ),
            ),
            const SizedBox(height: 22),
            // const Text("Sign Up / Login",
            //     style: TextStyle(
            //         color: AppColors.darkText,
            //         fontSize: 16,
            //         fontWeight: FontWeight.w700)),
            const SizedBox(height: 46),
          ],
        ),
      ),
    );
  }

  Widget _buildLoginButton([void Function()? onTap]) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 60,
        margin: const EdgeInsets.symmetric(horizontal: 30),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(99),
            gradient: const LinearGradient(
                begin: Alignment(1, 0),
                end: Alignment(-1, 0),
                colors: [
                  Color.fromRGBO(146, 163, 253, 1),
                  Color.fromRGBO(157, 206, 255, 1)
                ])),
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(AppImages.loginIcon, height: 25),
            const SizedBox(
              width: 10,
            ),
            const Text(
              "Login",
              style: TextStyle(
                  fontFamily: AppConstants.poppinsFonts,
                  color: AppColors.background,
                  fontSize: 16,
                  fontWeight: FontWeight.w700),
            )
          ],
        ),
      ),
    );
  }
}
