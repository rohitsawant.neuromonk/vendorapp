
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';

import '../../Controllers/business_info_controller.dart';
import '../../Models/business_search_model.dart';
import '../../Utils/app_colors.dart';
import '../../Utils/app_constants.dart';
import '../../Utils/app_images.dart';
import '../../Utils/custom_search_delegate.dart';
import '../../Utils/route_names.dart';
import '../../Utils/shared_prefs.dart';
import '../../Widgets/custom_back_button.dart';
import '../../Widgets/custom_text_field.dart';

class CreateAccountScreen extends StatefulWidget {
  const CreateAccountScreen({Key? key}) : super(key: key);

  @override
  State<CreateAccountScreen> createState() => _CreateAccountScreenState();
}

class _CreateAccountScreenState extends State<CreateAccountScreen> {
  final _businessInfoController = Get.put(BusinessInfoController());

  bool isOldUser = false;

  bool isSavingInfo = false;

  final RxString _businessName = "".obs;
  String? position;

  @override
  void initState() {
    super.initState();
    _fetchData();
  }

  _fetchData() async {
    isOldUser = await _businessInfoController.getBusinessInfo();

    if (isOldUser) {
      _businessInfoController.firstNameController.text =
          _businessInfoController.businessInfo?.firstName ?? "";
      _businessInfoController.lastNameController.text =
          _businessInfoController.businessInfo?.lastName ?? "";
      _businessInfoController.googleBusinessId.text =
          _businessInfoController.businessInfo?.googleBusinessId ?? "";
      position =
          _businessInfoController.businessInfo?.googleBusinessCoordinates;

      List<String>? mobileNo =
          (_businessInfoController.businessInfo?.mobileNumber)?.split(' ');

      if (mobileNo?.length == 2) {
        _businessInfoController.selectedCountryCode = mobileNo?[0];
        _businessInfoController.mobileController.text = mobileNo?[1] ?? '';
      } else {
        _businessInfoController.mobileController.text = mobileNo?[0] ?? '';
      }

      setState(() {});
    } else {
      _determinePosition().then((pos) {
        if (pos != null) position = '${pos.latitude},${pos.longitude}';
      });
    }
  }

  void _saveDetails() async {
    if (isSavingInfo) {
      Get.snackbar("Error!", "Wait while we're saving your info");
      return;
    }

    if (_businessInfoController.firstNameController.text.isEmpty ||
        _businessInfoController.lastNameController.text.isEmpty ||
        _businessInfoController.googleBusinessId.text.isEmpty ||
        _businessInfoController.mobileController.text.isEmpty) {
      Get.snackbar("Oops!", "Any of the fields cannot be left empty");
      return;
    }

    if (_businessInfoController.mobileController.text.length != 10) {
      Get.snackbar("Oops!", "Enter valid mobile number");
      return;
    }

    isSavingInfo = true;

    if (!isOldUser) {
      ///This we need only if we are removing Business Info Screen
      bool isSuccess = await _businessInfoController.saveBusinessInfo();

      if (isSuccess) {
        SharedPrefs.prefs.setString(
            SharedPrefsKeys.userStatus, AppConstants.paymentLeftStatus);
        Get.toNamed(RouteNames.PAYMENT_SCREEN);
      }
      isSavingInfo = false;
      return;

      ///This we need if we are keeping Business Info Screen
      // Get.toNamed(RouteNames.BUSINESS_INFO_SCREEN);
      // return;
    }

    bool isSuccess = await _businessInfoController.saveBusinessInfo();
    isSavingInfo = false;

    if (isSuccess) {
      _businessInfoController.businessInfo = null;

      Get.offAllNamed(RouteNames.BOTTOM_BAR_SCREEN);
    }
  }

  Future<Position?> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    try {
      serviceEnabled = await Geolocator.isLocationServiceEnabled();
      if (!serviceEnabled) {
        Get.snackbar('Oops!',
            'Location services are disabled. Please enable it and grant permission from settings');
        return null;
      }

      permission = await Geolocator.checkPermission();
      if (permission == LocationPermission.denied) {
        permission = await Geolocator.requestPermission();
        if (permission == LocationPermission.denied) {
          Get.snackbar('Oops!',
              'Location permissions are denied. Grant permission from settings');
          return null;
        }
      }

      if (permission == LocationPermission.deniedForever) {
        Get.snackbar('Oops!',
            'Location permissions are permanently denied, we cannot request permissions. Grant permission from settings');
        return null;
      }

      return await Geolocator.getCurrentPosition();
    } catch (error) {
      Get.snackbar('Error!',
          'Something went wrong! Make sure you have granted us the location permission');
      return null;
    }
  }

  void _getGoogleBusinessId() async {
    Get.dialog(
        WillPopScope(
          onWillPop: () async => false,
          child: const AlertDialog(
            content: LinearProgressIndicator(),
            title: Text('Loading Business...'),
          ),
        ),
        barrierDismissible: false);
    if (position == null) {
      await _determinePosition().then((pos) {
        if (pos != null) position = '${pos.latitude},${pos.longitude}';
      });

      if (position == null) {
        isSavingInfo = false;
        return;
      }
    }

    if (mounted) {
      final result = await showSearch<Results>(
          context: context, delegate: CustomSearchDelegate(position!));
      bool isBusinessIdExists =
          (_businessInfoController.businessInfo?.googleBusinessId ==
                  (result?.placeId ?? ""))
              ? false
              : await _businessInfoController
                  .checkIfBusinessIdExists(result?.placeId ?? "");
      Get.back();
      if (isBusinessIdExists) {
        Get.snackbar('Oops!', 'Business with this place id already exists');
        return;
      }
      if (result?.name != null) {
        _businessName.value = result!.name!;
        _businessInfoController.businessName = _businessName.value;
      }
      _businessInfoController.rating = (result?.rating ?? 0.0).toString();
      _businessInfoController.types = result?.types ?? [];
      _businessInfoController.businessCoordinates =
          '${result?.geometry?.location?.lat ?? ''},${result?.geometry?.location?.lng ?? ''}';
      _businessInfoController.googleBusinessId.text = result?.placeId ?? "";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: Container(
          padding: const EdgeInsets.only(bottom: 22, right: 10),
          child: InkWell(
              onTap: _saveDetails,
              child: Image.asset(AppImages.halfProgress, height: 60))),
      // resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const CustomBackButton(),
              Padding(
                padding: const EdgeInsets.only(left: 25, top: 20),
                child: Text(
                  "${isOldUser ? "Edit" : "Fill"} this details",
                  style: const TextStyle(
                      color: AppColors.darkText,
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                      fontFamily: AppConstants.poppinsFonts),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(top: 55, left: 25, right: 125),
                child: Text(
                  AppConstants.createAccountDes,
                  style: TextStyle(
                      color: AppColors.darkText,
                      fontSize: 12,
                      fontWeight: FontWeight.w400),
                ),
              ),
              const SizedBox(height: 50),
              CustomTextField(
                  hintText: "First Name",
                  textEditingController:
                      _businessInfoController.firstNameController),
              const SizedBox(height: 30),
              CustomTextField(
                  hintText: "last Name",
                  textEditingController:
                      _businessInfoController.lastNameController),
              const SizedBox(height: 30),
              Row(
                children: [
                  CountryCodePicker(
                      initialSelection:
                          _businessInfoController.selectedCountryCode,
                      onChanged: (value) => _businessInfoController
                          .selectedCountryCode = value.dialCode,
                      padding: const EdgeInsets.only(left: 15, right: 5)),
                  Expanded(
                    child: CustomTextField(
                        margin: const EdgeInsets.only(right: 15),
                        hintText: "Mobile Number",
                        textEditingController:
                            _businessInfoController.mobileController,
                        textInputType: TextInputType.phone),
                  ),
                ],
              ),
              const SizedBox(height: 30),
              CustomTextField(
                hintText: "Google Business Id",
                isReadOnly: true,
                textEditingController: _businessInfoController.googleBusinessId,
                onTap: _getGoogleBusinessId,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 30, top: 15),
                child: Obx(() => _businessName.value.isEmpty
                    ? const SizedBox()
                    : Text("Business Name: $_businessName")),
              )
            ],
          ),
        ),
      ),
    );
  }
}
