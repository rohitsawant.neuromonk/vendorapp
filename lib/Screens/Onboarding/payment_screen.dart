
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../Utils/app_colors.dart';
import '../../Utils/app_constants.dart';
import '../../Utils/app_images.dart';
import '../../Utils/route_names.dart';
import '../../Utils/shared_prefs.dart';
import '../../Widgets/custom_back_button.dart';
import '../../Widgets/payment_method.dart';
import '../../Widgets/promo_dialog.dart';

class PaymentScreen extends StatefulWidget {
  const PaymentScreen({Key? key}) : super(key: key);

  @override
  State<PaymentScreen> createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const CustomBackButton(),
                TextButton(
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (context) => PromoDialog(
                          onSubmit: (promoCode) {
                            if (promoCode != AppConstants.validPromoCode) {
                              Get.snackbar('Oops!', 'Invalid Promo Code');
                              return;
                            }

                            SharedPrefs.prefs.setString(
                                SharedPrefsKeys.userStatus,
                                AppConstants.readyToUseAppStatus);

                            Get.toNamed(RouteNames.SUCCESS_SCREEN);
                          },
                        ),
                      );
                    },
                    child: const Text(
                      "Have a promo code?",
                      style:
                          TextStyle(fontSize: 17, fontWeight: FontWeight.w500),
                    ))
              ],
            ),
            const Padding(
              padding: EdgeInsets.only(left: 25, top: 20, bottom: 50),
              child: Text(
                "Payment",
                style: TextStyle(
                    color: AppColors.darkText,
                    fontSize: 20,
                    fontWeight: FontWeight.w700,
                    fontFamily: AppConstants.poppinsFonts),
              ),
            ),
            const PaymentMethodButton(
                icon: AppImages.visaIcon,
                accountNumber: "2121 6352 8465 ****",
                gradientColors: [
                  Color.fromRGBO(98, 159, 226, 1),
                  Color.fromRGBO(45, 95, 182, 1)
                ]),
            const SizedBox(height: 20),
            const PaymentMethodButton(
              icon: AppImages.payoneerIcon,
              accountNumber: "2121 6352 8465 ****",
              gradientColors: [Color(0xFFF5985B), Color(0xFFD86318)],
              iconSize: 32,
            ),
            const Expanded(child: SizedBox()),
            Align(
                alignment: const Alignment(.85, 0),
                child: InkWell(
                    onTap: () => Get.toNamed(RouteNames.SUCCESS_SCREEN),
                    child: Image.asset(AppImages.fullProgress, height: 60))),
            const SizedBox(height: 40),
          ],
        ),
      ),
    );
  }
}
