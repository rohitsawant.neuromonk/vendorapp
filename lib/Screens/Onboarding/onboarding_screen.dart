import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:introduction_screen/introduction_screen.dart';

import '../../Utils/app_constants.dart';
import '../../Utils/app_images.dart';
import '../../Utils/route_names.dart';
import '../../Utils/shared_prefs.dart';
import '../../Widgets/custom_button.dart';

class OnBoardingScreen extends StatefulWidget {
  const OnBoardingScreen({
    super.key,
  });

  @override
  State<OnBoardingScreen> createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {
  final PageController pageController = PageController();

  int _currentPage = 0;

  final _carouselImages = [
    AppImages.growBusinessIllustration,
    // AppConstants.firsttext
  ];
  final _carouselImages1 = [
    AppImages.offerCreateIllustration,
    // AppConstants.secondtext
  ];
  final _carouselImages2 = [
    AppImages.verifyIllustration,
    // AppConstants.thridtext
  ];

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: SafeArea(
//         child: Padding(
//           padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 80),
  //   child: Column(
  //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //     children: [
  //       Container(
  //         width: 380,
  //         decoration: BoxDecoration(
  //           border: Border.all(
  //             color: Colors.grey.withOpacity(0.2),
  //             style: BorderStyle.solid,
  //             width: 2.0,
  //           ),
  //           color: Colors.transparent,
  //           borderRadius: BorderRadius.circular(30.0),
  //         ),
  //         child: Column(
  //           children: [
  //             SizedBox(
  //               height: 550,
  //               width: 400,
  //               child: PageView(
  //                   onPageChanged: (value) => _currentPage = value,
  //                   controller: pageController,
  //                   children: _carouselImages
  //                       .map<Widget>((img) => Padding(
  //                             padding: const EdgeInsets.all(14),
  //                             child: Column(
  //                               children: [
  //                                 Padding(
  //                                   padding:
  //                                       const EdgeInsets.only(top: 20),
  //                                   child: Column(
  //                                     children: [
  //                                       Image.asset(img),
  //                                     ],
  //                                   ),
  //                                 ),
  //                               ],
  //                             ),
  //                           ))
  //                       .toList()),
  //             ),
  //           ],
  //         ),
  //       ),
  //       Padding(
  //         padding: const EdgeInsets.only(bottom: 0),
  //         child: CustomButton(
  //           label: 'Next',
  //           onTap: () {
  //             if (_currentPage == _carouselImages.length - 1) {
  //               SharedPrefs.prefs.setString(SharedPrefsKeys.userStatus,
  //                   AppConstants.loginLeftStatus);
  //               Get.toNamed(RouteNames.BOTTOM_BAR_SCREEN);
  //             }
  //             pageController.nextPage(
  //                 duration: const Duration(milliseconds: 800),
  //                 curve: Curves.fastOutSlowIn);
  //           },
  //         ),
  //       ),
  //     ],
  //   ),
  // ),
//       ),
//     );
//   }
// }
  final _introKey = GlobalKey<IntroductionScreenState>();

  @override
  Widget build(BuildContext context) {
    return IntroductionScreen(
      key: _introKey,
      pages: [
        PageViewModel(
          title: '',
          bodyWidget: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 380,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey.withOpacity(0.2),
                    style: BorderStyle.solid,
                    width: 2.0,
                  ),
                  color: Colors.transparent,
                  borderRadius: BorderRadius.circular(30.0),
                ),
                child: Column(
                  children: [
                    SizedBox(
                      height: 550,
                      width: 400,
                      child: PageView(
                          onPageChanged: (value) => _currentPage = value,
                          controller: pageController,
                          children: _carouselImages
                              .map<Widget>((img) => Padding(
                                    padding: const EdgeInsets.all(14),
                                    child: Column(
                                      children: [
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(top: 20),
                                          child: Column(
                                            children: [
                                              Image.asset(img),
                                              Text(AppConstants.firsttext)
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ))
                              .toList()),
                    ),
                  ],
                ),
              ),
              // Padding(
              //   padding: const EdgeInsets.only(bottom: 0),
              //   child: CustomButton(
              //     label: 'Next',
              //     onTap: () {
              //       if (_currentPage == _carouselImages.length - 1) {
              //         SharedPrefs.prefs.setString(SharedPrefsKeys.userStatus,
              //             AppConstants.loginLeftStatus);
              //         Get.toNamed(RouteNames.BOTTOM_BAR_SCREEN);
              //       }
              //       pageController.nextPage(
              //           duration: const Duration(milliseconds: 800),
              //           curve: Curves.fastOutSlowIn);
              //     },
              //   ),
              // ),
            ],
          ),
        ),
        PageViewModel(
          title: '',
          bodyWidget: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 380,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey.withOpacity(0.2),
                    style: BorderStyle.solid,
                    width: 2.0,
                  ),
                  color: Colors.transparent,
                  borderRadius: BorderRadius.circular(30.0),
                ),
                child: Column(
                  children: [
                    SizedBox(
                      height: 550,
                      width: 400,
                      child: PageView(
                          onPageChanged: (value) => _currentPage = value,
                          controller: pageController,
                          children: _carouselImages1
                              .map<Widget>((img) => Padding(
                                    padding: const EdgeInsets.all(14),
                                    child: Column(
                                      children: [
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(top: 20),
                                          child: Column(
                                            children: [
                                              Image.asset(img),
                                              Text(AppConstants.secondtext)
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ))
                              .toList()),
                    ),
                  ],
                ),
              ),
              // Padding(
              //   padding: const EdgeInsets.only(bottom: 0),
              //   child: CustomButton(
              //     label: 'Next',
              //     onTap: () {
              //       if (_currentPage == _carouselImages.length - 1) {
              //         SharedPrefs.prefs.setString(SharedPrefsKeys.userStatus,
              //             AppConstants.loginLeftStatus);
              //         Get.toNamed(RouteNames.BOTTOM_BAR_SCREEN);
              //       }
              //       pageController.nextPage(
              //           duration: const Duration(milliseconds: 800),
              //           curve: Curves.fastOutSlowIn);
              //     },
              //   ),
              // ),
            ],
          ),
        ),
        PageViewModel(
          title: '',
          bodyWidget: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 380,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey.withOpacity(0.2),
                    style: BorderStyle.solid,
                    width: 2.0,
                  ),
                  color: Colors.transparent,
                  borderRadius: BorderRadius.circular(30.0),
                ),
                child: Column(
                  children: [
                    SizedBox(
                      height: 550,
                      width: 400,
                      child: PageView(
                          onPageChanged: (value) => _currentPage = value,
                          controller: pageController,
                          children: _carouselImages2
                              .map<Widget>((img) => Padding(
                                    padding: const EdgeInsets.all(14),
                                    child: Column(
                                      children: [
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(top: 20),
                                          child: Column(
                                            children: [
                                              Image.asset(img),
                                              Text(AppConstants.thridtext)
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ))
                              .toList()),
                    ),
                  ],
                ),
              ),
           
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(
                    width: 200,
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 0),
                      child: CustomButton(
                        label: 'Next',
                        onTap: () {
                          // if (_currentPage == _carouselImages.length - 1) {
                            SharedPrefs.prefs.setString(SharedPrefsKeys.userStatus,
                                AppConstants.loginLeftStatus);
                            Get.toNamed(RouteNames.BOTTOM_BAR_SCREEN);
                          // }
                          // pageController.nextPage(
                          //     duration: const Duration(milliseconds: 800),
                          //     curve: Curves.fastOutSlowIn);
                        },
                      ),
                    ),
                  ),
                ],
              ),
              
            ],
          ),
        )
      ],
      showNextButton: false,
      showDoneButton: false,
    );
  }
}
