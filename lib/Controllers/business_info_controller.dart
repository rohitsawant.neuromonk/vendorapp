import 'dart:convert';
import 'dart:developer';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;

import '../Models/business_info.dart';
import '../Models/business_search_model.dart';
import '../Utils/app_constants.dart';

class BusinessInfoController extends GetxController {
  final TextEditingController firstNameController = TextEditingController();
  final TextEditingController lastNameController = TextEditingController();
  final TextEditingController mobileController = TextEditingController();
   final TextEditingController otpController = TextEditingController();
  final TextEditingController businessTitleController = TextEditingController();
  final TextEditingController businessCategoryController =
      TextEditingController();
  final TextEditingController businessContactController =
      TextEditingController();
  final TextEditingController businessLocationController =
      TextEditingController();
  final TextEditingController businessReviewController =
      TextEditingController();
  final TextEditingController googleBusinessId = TextEditingController();
  String businessName = '';
  String businessCoordinates = '';
  final RxBool _isLoading = false.obs;

  RxBool get isLoading => _isLoading;

  RxString userName = "Sir".obs;
  String? selectedCountryCode;
  String rating = '0';
  List<String> types = [];

  BusinessInfo? businessInfo;

  Future<bool> getBusinessInfo() async {
    _isLoading.value = true;

    try {
      String? uid = FirebaseAuth.instance.currentUser?.uid;

      if (uid == null) {
        throw "User doesn't exists";
      }

      final snapshot =
          await FirebaseFirestore.instance.doc("businesses/$uid").get();
      log(snapshot.get("info").toString(), name: 'Business Info');
      businessInfo = BusinessInfo.fromJson(snapshot.get("info") as Map);
      return true;
    } catch (e) {
      debugPrint("Business Info Error ---> $e");
      return false;
    }
  }

  Future<bool> saveBusinessInfo() async {
    try {
      String? uid = FirebaseAuth.instance.currentUser?.uid;

      if (uid == null) {
        throw "User doesn't exists";
      }

      await FirebaseFirestore.instance.doc("businesses/$uid").set({
        "business_id": googleBusinessId.text,
        "info": BusinessInfo(
                rating: rating,
                types: types,
                googleBusinessName: businessName,
                googleBusinessCoordinates: businessCoordinates,
                lastName: lastNameController.text,
                mobileNumber:
                    '${selectedCountryCode ?? ''} ${mobileController.text}',
                firstName: firstNameController.text,
                googleBusinessId: googleBusinessId.text)
            .toJson()
      }, SetOptions(merge: true));

      Get.snackbar("Hurray", "Successfully saved business info");
      return true;
    } catch (e) {
      Get.snackbar("Oops!", "Something went wrong while saving business info");
      debugPrint("Error Saving Business Info ----> $e");
      return false;
    }
  }

  Future<bool> updateOffersClaimed() async {
    try {
      if (businessInfo == null) {
        throw 'Business Info is null';
      }

      String? uid = FirebaseAuth.instance.currentUser?.uid;

      if (uid == null) {
        throw "User doesn't exists";
      }

      businessInfo!.totalOffersClaimed =
          (businessInfo?.totalOffersClaimed ?? 0) + 1;

      await FirebaseFirestore.instance.doc("businesses/$uid").set({
        "info": {
          'total_offers_claimed': businessInfo?.totalOffersClaimed ?? 0,
        }
      }, SetOptions(merge: true));

      return true;
    } catch (e) {
      log(e.toString(), name: 'Update Offers Claimed');
      return false;
    }
  }

  Future<BusinessSearchModel?> getBusinessIds(
      String keyword, String location) async {
    _isLoading.value = true;
    try {
      Uri uri = Uri.parse(
          "${AppConstants.googleApisBaseUri}nearbysearch/json?location=$location&radius=1000&keyword=$keyword&key=${AppConstants.googleMapsKey}");
      debugPrint(uri.toString());
      final response = await http.get(uri);
      Map jsonBody = jsonDecode(response.body);

      if (response.statusCode != 200 || jsonBody["status"] != "OK") {
        throw response.reasonPhrase ?? 'Unknown Error';
      }

      log(jsonBody.toString(), name: 'Get Business Id Response');

      return BusinessSearchModel.fromJson(jsonBody);
    } catch (e) {
      log(e.toString(), name: 'Get Business Id Error');
      return null;
    }
  }

  Future<void> deleteBusiness() async {
    try {
      String? uid = FirebaseAuth.instance.currentUser?.uid;
      if (uid == null) throw "User doesn't exists";

      await FirebaseAuth.instance.currentUser!.delete();
      await FirebaseAuth.instance.signOut();
      await GoogleSignIn().signOut();
      await FirebaseFirestore.instance.doc("businesses/$uid").delete();
    } catch (e) {
      Get.snackbar("Oops!", "Something went wrong while deleting account");
      log(e.toString(), name: 'Delete Business Error');
    }
  }

  Future<bool> checkIfBusinessIdExists(String businessId) async {
    try {
      final response = await FirebaseFirestore.instance
          .collection('businesses')
          .where('business_id', isEqualTo: businessId)
          .get();

      final data = response.docs[0].data();
      log(data.toString(), name: 'checkIfBusinessIdExists');

      return true;
    } catch (e) {
      log(e.toString(), name: 'checkIfBusinessIdExists');
      return false;
    }
  }
}
