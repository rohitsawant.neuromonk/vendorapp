import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';

class AuthController extends GetxController {
  Future<bool> signInWithGoogle() async {
    try {
      final signIn = await GoogleSignIn().signIn();
      final googleAuth = await signIn?.authentication;

      if (googleAuth == null) {
        throw "";
      }

      final credential = GoogleAuthProvider.credential(
          accessToken: googleAuth.accessToken, idToken: googleAuth.idToken);
      await FirebaseAuth.instance.signInWithCredential(credential);
      return true;
    } catch (e) {
      debugPrint("Error while signing in ----> $e");
      Get.snackbar("Oops!", "Something went wrong");
      return false;
    }
  }
}

