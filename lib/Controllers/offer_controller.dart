
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../Models/offer.dart';

class OfferController extends GetxController {
  final RxBool _isLoading = false.obs;

  RxBool get isLoading => _isLoading;

  List<Offer> offerList = <Offer>[];

  Future<bool> getOffers() async {
    offerList.clear();
    try {
      String? uid = FirebaseAuth.instance.currentUser?.uid;

      if (uid == null) {
        throw "User not found";
      }

      final snapshot = await FirebaseFirestore.instance
          .collection("businesses")
          .doc(uid)
          .get();

      List list = snapshot.get("offers") as List;

      offerList = list.map<Offer>((e) => Offer.fromJson(e)).toList();
      return true;
    } catch (e) {
      debugPrint("Error while fetching offers ---> $e");
      return false;
    }
  }

  Future<bool> deleteOffer(int offerIndex) async {
    _isLoading.value = true;
    try {
      offerList.removeAt(offerIndex);

      List<Map> newOfferList = offerList.map<Map>((e) => e.toJson()).toList();

      String? uid = FirebaseAuth.instance.currentUser?.uid;

      if (uid == null) {
        throw "User not found";
      }

      await FirebaseFirestore.instance
          .collection("businesses")
          .doc(uid)
          .set({"offers": newOfferList}, SetOptions(merge: true));
      _isLoading.value = false;
      return true;
    } catch (e) {
      Get.snackbar("Oops!", "Something went wrong while deleting offer");
      debugPrint("Error while adding offer ---> $e");
      _isLoading.value = false;
      return false;
    }
  }

  Future<bool> changeOfferList(Offer offer, [int? offerIndex]) async {
    _isLoading.value = true;
    try {
      if (offerIndex == null) {
        offerList.add(offer);
      } else {
        offerList[offerIndex] = offer;
      }

      List<Map> newOfferList = offerList.map<Map>((e) => e.toJson()).toList();

      String? uid = FirebaseAuth.instance.currentUser?.uid;

      if (uid == null) {
        throw "User not found";
      }
      debugPrint("Fetching Offers");
      await FirebaseFirestore.instance
          .collection("businesses")
          .doc(uid)
          .set({"offers": newOfferList}, SetOptions(merge: true));
      debugPrint("Offers Fetched");
      _isLoading.value = false;
      debugPrint("Loading done ---> ${_isLoading.value}");
      return true;
    } catch (e) {
      Get.snackbar("Oops!",
          "Something went wrong while ${offerIndex == null ? "creating" : "editing"} offer");
      debugPrint("Error while adding offer ---> $e");
      _isLoading.value = false;
      return false;
    }
  }
}
