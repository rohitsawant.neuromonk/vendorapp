import 'dart:async';
import 'dart:developer';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:vendorapp/Screens/Onboarding/onboarding_screen.dart';
import 'Screens/Home/splash_screen.dart';
import 'Utils/app_colors.dart';
import 'Utils/app_constants.dart';
import 'Utils/app_images.dart';
import 'Utils/app_router.dart';
import 'Utils/route_names.dart';
import 'Utils/shared_prefs.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await SharedPrefs.initialize();

  runApp(GetMaterialApp(
    onGenerateRoute: AppRouter.generateRoute,
    debugShowCheckedModeBanner: false,
    theme: ThemeData(
        scaffoldBackgroundColor: AppColors.background,
        fontFamily: AppConstants.segoeFont),
    // home: OnBoardingScreen(),
    initialRoute: getInitialRoute(),
  ));
}

String getInitialRoute() {
  String userStatus = SharedPrefs.prefs.getString(SharedPrefsKeys.userStatus) ??
      AppConstants.onBoardingLeftStatus;

  switch (userStatus) {
    // case AppConstants.splashscreen:
    //   return RouteNames.SPLASG_SCREEN;

    case AppConstants.onBoardingLeftStatus:
      return RouteNames.WELCOME_SCREEN;

    case AppConstants.loginLeftStatus:
      return RouteNames.ONBOARDING_SCREEN;

    case AppConstants.profileCreationLeftStatus:
      return RouteNames.CREATE_ACCOUNT_SCREEN;

    case AppConstants.paymentLeftStatus:
      return RouteNames.PAYMENT_SCREEN;

    default:
      return RouteNames.BOTTOM_BAR_SCREEN;
  }
}
