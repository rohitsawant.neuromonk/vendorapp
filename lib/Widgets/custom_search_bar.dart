import 'package:flutter/material.dart';

import '../Utils/app_colors.dart';
import '../Utils/app_images.dart';

class CustomSearchBar extends StatelessWidget {
  final String hintText;
  final TextEditingController? controller;
  final void Function(String keyword)? onSearch;
  const CustomSearchBar(
      {Key? key, required this.hintText, this.controller, this.onSearch})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      margin: const EdgeInsets.symmetric(horizontal: 13),
      padding: const EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
          color: AppColors.lightOrange.withOpacity(.15),
          borderRadius: BorderRadius.circular(15)),
      alignment: Alignment.center,
      child: TextField(
        controller: controller,
        onChanged: onSearch,
        decoration: InputDecoration(
            hintStyle: TextStyle(
                color: AppColors.darkOrange.withOpacity(.4), fontSize: 14),
            hintText: hintText,
            border: InputBorder.none,
            prefixIconConstraints: BoxConstraints.tight(const Size(45, 24)),
            prefixIcon: Align(
              alignment: Alignment.centerLeft,
              child: Image.asset(
                AppImages.searchIcon,
              ),
            )),
      ),
    );
  }
}
