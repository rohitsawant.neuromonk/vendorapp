import 'package:flutter/material.dart';

import '../Utils/app_colors.dart';
import 'action_tile.dart';

class ActionBox extends StatelessWidget {
  final List<ActionTile> actionTiles;
  final String label;
  const ActionBox({Key? key, required this.actionTiles, required this.label})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 25),
      decoration: BoxDecoration(
          color: AppColors.background,
          boxShadow: const [
            BoxShadow(
                color: Color.fromRGBO(29, 22, 23, 0.07000000029802322),
                offset: Offset(0, 10),
                blurRadius: 40)
          ],
          borderRadius: BorderRadius.circular(16)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // Align(
          //   alignment: Alignment.centerLeft,
          //   child: Text(
          //     label,
          //     style: const TextStyle(
          //         color: AppColors.darkText,
          //         fontSize: 18,
          //         fontWeight: FontWeight.w700),
          //   ),
          // ),
          // const SizedBox(height: 10),
          ...actionTiles,
        ],
      ),
    );
  }
}
