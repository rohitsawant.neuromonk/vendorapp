
import 'package:flutter/material.dart';
// import 'package:share_plus/share_plus.dart';

import '../Models/offer.dart';
import '../Utils/app_colors.dart';

class OfferBox extends StatelessWidget {
  final void Function()? onTap;
  final void Function()? onDelete;
  final String image;
  final Offer offer;
  const OfferBox(
      {Key? key,
      this.onTap,
      required this.image,
      required this.offer,
      this.onDelete})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    String offerStatus = offer.isActive == "true" ? "Active" : "Inactive";
    if (offer.dateRange != null && offerStatus != "Inactive") {
      DateTime dateTime =
          DateTime.parse((offer.dateRange ?? "").split(" to ")[1]);
      if (dateTime.isBefore(DateTime.now())) {
        offerStatus = "Expired";
      }
    }
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 14, vertical: 10),
        padding:
            const EdgeInsets.only(left: 14, right: 23, top: 19, bottom: 13),
        decoration: BoxDecoration(
          color: offerStatus != "Active"
              ? AppColors.lightGrey
              : AppColors.background,
          borderRadius: BorderRadius.circular(22),
          boxShadow: const [
            BoxShadow(
                color: Color.fromRGBO(90, 108, 234, 0.07000000029802322),
                offset: Offset(12, 26),
                blurRadius: 50)
          ],
        ),
        child: Row(
          children: [
            Container(
              height: 62,
              width: 62,
              decoration: BoxDecoration(
                  color: AppColors.background,
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                      image: AssetImage(image),
                      colorFilter: offerStatus != "Active"
                          ? const ColorFilter.mode(
                              AppColors.disabledGrey, BlendMode.color)
                          : null,
                      fit: BoxFit.cover)),
              // child: ,
            ),
            const SizedBox(width: 18),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  offer.title ?? "No Title",
                  style: const TextStyle(
                      color: AppColors.darkText,
                      fontSize: 15,
                      fontWeight: FontWeight.w700),
                ),
                // Text(
                //   "Traded Value",
                //   style: TextStyle(
                //       color: AppColors.darkGrey.withOpacity(.5),
                //       fontSize: 14,
                //       fontWeight: FontWeight.w400),
                // ),
                // Text(
                //   "\$ ${offer.price ?? 0}",
                //   style: TextStyle(
                //       color: offerStatus != "Active"
                //           ? AppColors.disabledGrey
                //           : AppColors.lightGreen,
                //       fontSize: 19,
                //       fontWeight: FontWeight.w700),
                // ),
              ],
            ),
            const Expanded(child: SizedBox()),
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  height: 29,
                  width: 79,
                  margin: const EdgeInsets.only(top: 15),
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: const Alignment(-0.85, 0.131),
                          end: const Alignment(0.4, 0.111),
                          colors: offerStatus != "Active"
                              ? [AppColors.disabledGrey, AppColors.disabledGrey]
                              : [
                                  const Color.fromRGBO(83, 231, 139, 1),
                                  const Color.fromRGBO(20, 190, 119, 1)
                                ]),
                      borderRadius: BorderRadius.circular(17.5)),
                  alignment: Alignment.center,
                  child: Text(
                    offerStatus,
                    style: const TextStyle(
                        color: AppColors.background,
                        fontSize: 12,
                        fontWeight: FontWeight.w700),
                  ),
                ),
                Row(
                  children: [
                    IconButton(
                        onPressed: onDelete,
                        icon: const Icon(
                          Icons.delete,
                          size: 20,
                        )),
                    IconButton(
                        onPressed: () {
                          // Share.share("Welcome to Breviewer");
                        },
                        icon: const Icon(
                          Icons.share,
                          size: 20,
                        ))
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
