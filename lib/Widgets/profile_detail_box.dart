
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../Utils/app_colors.dart';
import '../Utils/app_images.dart';

class ProfileDetailBox extends StatelessWidget {
  final String userName;
  final String offersClaimed;
  final String offersViewed;
  final void Function()? onEdit;
  const ProfileDetailBox(
      {Key? key,
      required this.userName,
      this.onEdit,
      required this.offersClaimed,
      required this.offersViewed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final user = FirebaseAuth.instance.currentUser;
    return Column(
      children: [
        Row(
          children: [
            // Image.asset(AppImages.girlProfile, height: 55),
            // Image.network(),
            CircleAvatar(
              radius: 27.5,
              backgroundColor: AppColors.lightBlue,
              backgroundImage: const AssetImage(AppImages.girlProfile),
              foregroundImage: NetworkImage(user?.photoURL ?? ""),
            ),
            const SizedBox(width: 15),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  userName,
                  style: const TextStyle(
                      color: AppColors.darkText,
                      fontSize: 16,
                      fontWeight: FontWeight.w600),
                ),
                Text(
                  user?.email ?? "",
                  style: const TextStyle(
                      color: AppColors.darkGrey,
                      fontSize: 12,
                      fontWeight: FontWeight.w500),
                )
              ],
            ),
            const Expanded(child: SizedBox()),
            GestureDetector(
              onTap: onEdit,
              child: Container(
                width: 50,
                height: 35,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(99),
                  gradient: const LinearGradient(
                      begin: Alignment(-1, 0),
                      end: Alignment(1.7, 0),
                      colors: [
                        Color.fromRGBO(98, 159, 226, 1),
                        Color.fromRGBO(45, 95, 182, 1)
                      ]),
                ),
                alignment: Alignment.center,
                child: const Text(
                  "Edit",
                  style: TextStyle(
                      color: AppColors.background,
                      fontSize: 14,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ),
          ],
        ),
        const SizedBox(height: 30),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _buildDetailBox(offersViewed, "Total Offers\nViewed"),
            _buildDetailBox(offersClaimed, "Total Offers\nClaimed"),
          ],
        ),
      ],
    );
  }

  Widget _buildDetailBox(String value, String label) {
    return Container(
      height: 75,
      width: 155,
      decoration: BoxDecoration(
        color: AppColors.background,
        borderRadius: BorderRadius.circular(16),
        boxShadow: const [
          BoxShadow(
              color: Color.fromRGBO(29, 22, 23, 0.07000000029802322),
              offset: Offset(0, 10),
              blurRadius: 40)
        ],
      ),
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            value,
            style: const TextStyle(
                color: AppColors.lightBlue,
                fontSize: 16,
                fontWeight: FontWeight.w600),
          ),
          const SizedBox(height: 2),
          Text(
            label,
            textAlign: TextAlign.center,
            style: const TextStyle(
                color: AppColors.darkText,
                fontSize: 12,
                fontWeight: FontWeight.w500),
          )
        ],
      ),
    );
  }
}
