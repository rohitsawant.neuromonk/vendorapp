import 'package:flutter/material.dart';

import 'custom_button.dart';

class PromoDialog extends StatelessWidget {
  final void Function(String promoCode) onSubmit;
  PromoDialog({Key? key, required this.onSubmit}) : super(key: key);
  final TextEditingController controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text(
        "Enter Promo Code",
        style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),
      ),
      content: TextField(
        controller: controller,
        decoration: const InputDecoration(border: OutlineInputBorder()),
      ),
      actions: [
        CustomButton(
          label: "Apply",
          onTap: () => onSubmit(controller.text),
        )
      ],
    );
  }
}
