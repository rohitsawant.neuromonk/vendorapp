import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import '../Utils/app_constants.dart';
import '../Utils/app_images.dart';
import '../Utils/route_names.dart';
import '../Utils/shared_prefs.dart';
import 'custom_button.dart';

class WidgetOnBoarding extends StatelessWidget {
  const WidgetOnBoarding({super.key});

  @override
  Widget build(BuildContext context) {
     final PageController pageController = PageController();

  int _currentPage = 0;

  final _carouselImages = [
  
    AppImages.growBusinessIllustration,
    // AppImages.offerCreateIllustration,
    // AppImages.verifyIllustration,
  ];

    return
      Padding(
        padding: const EdgeInsets.only(top: 50),
        child: Container(
                  width: 380,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.grey.withOpacity(0.4),
                      style: BorderStyle.solid,
                      width: 2.0,
                    ),
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                  child: Column(
                    children: [
                      Image.asset("assets/images/first_screen.png"),
                      Text('"Explore   Nearby   Offers"  is  like  your'
      '\n personal treasure map to finding super'
      '\n cool deals  and  special  discounts  that'
      '\n are just around the corner.')
                      // SizedBox(
                      //   height: 550,
                      //   width: 400,
                      //   child: PageView(
                      //       onPageChanged: (value) => _currentPage = value,
                      //       controller: pageController,
                      //       children: _carouselImages
                      //           .map<Widget>((img) => Padding(
                      //                 padding: const EdgeInsets.all(14),
                      //                 child: Column(
                      //                   children: [
                      //                     Padding(
                      //                       padding: const EdgeInsets.only(top: 20),
                      //                       child: Column(
                      //                         children: [
                                                    
                      //                           Image.asset(img),
                      //                     // Text(AppConstants.firsttext),
                      //                         ],
                      //                       ),
                                            
                      //                     ),
                                          
                      //                     // SizedBox(
                      //                     //   height: 150,
                      //                     //   width: 300,
                      //                     //   child: PageView(
                      //                     //     onPageChanged: (value) =>
                      //                     //         _currentPage = value,
                      //                     //     controller: pageController,
                      //                     //     children: _carouselImage
                      //                     //         .map<Widget>((img) => Padding(
                      //                     //               padding:
                      //                     //                   const EdgeInsets.all(
                      //                     //                       14),
                      //                     //               child: Column(
                      //                     //                 children: [Text(img)],
                      //                     //               ),
                      //                     //             ))
                      //                     //         .toList(),
                      //                     //   ),
                      //                     // )
                      //                   ],
                      //                 ),
                      //               ))
                      //           .toList()),
                      // ),
                    ],
                  ),
                  // padding: EdgeInsets.all(20),
                  // child:
                  // SizedBox(
                  //   height: 426,
                  //   // height: MediaQuery.of(context).size.height * .7,
                  //   child:
                  //  PageView(
                  //     onPageChanged: (value) => _currentPage = value,
                  //     controller: pageController,
                  //     children: _carouselImages
                  //         .map<Widget>((img) => Padding(
                  //               padding: const EdgeInsets.all(14),
                  //               child: Column(
                  //                 children: [
                  //                   Image.asset(img),
                  //         Text('')
                  //                 ],
                  //               ),
                  //             ))
                  //         .toList()),
                  // ),
                ),
      );

  }
}