class AppConstants {
  //User Status
   static const String splashscreen = 'splashscreen';
  static const String onBoardingLeftStatus = 'onboarding_left';
  static const String loginLeftStatus = 'login_left';
  static const String profileCreationLeftStatus = 'profileCreationLeft';
  static const String paymentLeftStatus = 'payment_done';
  static const String readyToUseAppStatus = 'ready_to_use_app';

  //Promo Codes
  static const String validPromoCode = 'BReviewerFree101';

  //  Fonts
  static const String segoeFont = "Segoe";
  static const String poppinsFonts = "Poppins";

  static const String googleApisBaseUri =
      "https://maps.googleapis.com/maps/api/place/";

  static const String googleMapsKey = 'AIzaSyA6znuTErJZHInnWhpOb3UD-XOuSJqe6_c';

  //  Texts
  static const String createAccountDes =
      "This data will be displayed in your account profile for security";
  static const String businessInfoDes =
      "This data will be displayed in your account profile for security";

  static const String appSecretKey = "breviewer-qr-key";

    static const String firsttext =
      '"Explore   Nearby   Offers"  is  like  your'
      '\n personal treasure map to finding super'
      '\n cool deals  and  special  discounts  that'
      '\n are just around the corner.';

 static const String secondtext =
      '"Redeem Offer" is your golden ticket to'
      '\n grabbing those amazing deals. Scan'
      '\n your code and it''s your way of saying,'
      '\n "Yes, please!"';
 static const String thridtext =
      '"Post a Review" is your chance to give a'
      '\n shoutout to a business whose offer'
      '\n you''ve enjoyed.';




  static const String privacyPolicy =
      """We take your privacy seriously. That’s why we are open and transparent about what we do with your personal information.

1.. What information do we collect?
We collect data like the details you provide when registering to use our app, and any contact details you provide when contacting customer services.

2. What do we use it for?
We’ll only use your data for the reasons you provided it in the first place. This could be to verify your identity while validating your review or to respond to a query you raised with our Customer Services Team.
Also, to personalise and improve our products and services and your overall customer experience, in particular by improving our existing technologies and developing new products and services and employing profiling technology (unless we notify you separately thereof, this will not include automated individual decisions that have legal effects for you or similarly significantly affect you);

3. Do we share the information we collect?
We may share your personal information with other businesses registered on our app, our suppliers and our franchisees. For example, where a BReviewer’s marketing agency is administering a competition on behalf of BReviewer’s  your data may be shared with that agency - but only used for the purposes that you shared it with us for. It will always be transferred and stored securely. We won’t sell your data to third parties. Ever.

4. Can I change my newsletter subscriptions?
When you subscribe to receive emails from us, we will only contact you with the newsletters you expressed an interest in. You can change your preferences any time by accessing your personalised preference centre via any of the emails you receive from us.
""";

  static const String aboutUs = """About us:


India has a large and diverse business landscape, with millions of registered and unregistered businesses operating across various sectors and industries. The majority of businesses in India are SMEs, which play a crucial role in driving economic growth and creating employment opportunities.

The majority of businesses in India are small and medium-sized enterprises (SMEs). 
India is home to a vibrant and growing startup ecosystem, with thousands of new startups emerging each year. According to the 2021 Nasscom-Zinnov Startup Ecosystem Report, India is now home to more than 12,500 startups, making it the third-largest startup ecosystem in the world after the United States and China.

However, such businesses often face hurdles to compete in the market whether it is due to low marketing budget, or lack of Understanding customer needs, not having Online presence and mainly Build credibility among customers.

We recognise the unique opportunity to help such businesses to rise up and stand tall among competitors and at the same time voice out the customers opinions about any such businesses products.

BReviewer is an One-stop platform for promoting customer opinions and winning real-time offers on deals and discounts for nearby businesses or services registered on our application by sending a single review for them.

Customer reviews are a powerful tool for any business in India to build trust, understand customer needs, differentiate from competitors, and improve their online presence, all while operating on limited budgets.
""";
}
