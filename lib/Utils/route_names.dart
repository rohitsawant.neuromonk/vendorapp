// ignore_for_file: constant_identifier_names

class RouteNames {
  static const String ONBOARDING_SCREEN = "onboarding_screen";
  static const String WELCOME_SCREEN = "welcome_screen";
  static const String SPLASG_SCREEN = "splash_screen";
  static const String BUSINESS_INFO_SCREEN = "business_info_screen";
  static const String CREATE_ACCOUNT_SCREEN = "create_account_screen";
  static const String PAYMENT_SCREEN = "payment_screen";
  static const String SUCCESS_SCREEN = "success_screen";
  static const String PERSONAL_DATA_SCREEN = "personal_data_screen";
  static const String BOTTOM_BAR_SCREEN = "bottom_bar_screen";
  static const String NOTIFICATION_SCREEN = "notification_screen";
  static const String EDIT_OFFER_SCREEN = "edit_offer_screen";
  static const String PRIVACY_POLICY_SCREEN = "privacy_policy_screen";
  static const String ABOUT_US_SCREEN = "about_us_screen";
}
