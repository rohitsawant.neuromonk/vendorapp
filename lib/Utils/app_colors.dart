import 'package:flutter/material.dart';

class AppColors {
  static const Color background = Color(0xFFFFFFFF);
  static const Color darkText = Color(0xFF09051C);
  static const Color darkGrey = Color(0xff7b6f72);
  static const Color lightGrey = Color(0xFFF6F6F6);
  static const Color disabledGrey = Color(0xFFD9D9D9);
  static const Color lightBlue = Color(0xFF629FE2);
  static const Color purple = Color(0xFFC58BF2);
  static const Color lightOrange = Color(0xFFF9A84D);
  static const Color darkOrange = Color(0xFFDA6317);
  static const Color lightGreen = Color(0xFF53E8B8);
  static const Color green = Color(0xFF15BE77);
}
