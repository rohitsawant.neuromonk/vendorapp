class AppImages {
  static const String imageBasePath = "assets/images";
  static const String iconBasePath = "assets/icons";

  // Images
  static const String girlProfile = "$imageBasePath/girl_profile.png";
  static const String success = "$imageBasePath/success.png";
  static const String food = "$imageBasePath/food.png";

  //Illustrations
  static const String rateIllustration = "$imageBasePath/welcome.png";
   static const String knoffslogo = "$imageBasePath/logo1.png";
  static const String growBusinessIllustration =
      "$imageBasePath/first_screen.png";
  static const String offerCreateIllustration =
      "$imageBasePath/second_screen.png";
  static const String verifyIllustration =
      "$imageBasePath/third_screen.png";

      

  //Icons
  static const String loginIcon = "$iconBasePath/login_icon.png";
  static const String halfProgress = "$iconBasePath/half_progress.png";
  static const String fullProgress = "$iconBasePath/full_progress.png";
  static const String googleIcon = "$iconBasePath/google_icon.png";
  static const String visaIcon = "$iconBasePath/visa_icon.png";
  static const String payoneerIcon = "$iconBasePath/payoneer_icon.png";
  static const String homeActive = "$iconBasePath/home_active.png";
  static const String homeInActive = "$iconBasePath/home_inactive.png";
  static const String userActive = "$iconBasePath/user_active.png";
  static const String userInActive = "$iconBasePath/user_inactive.png";
  static const String analyticsIcon = "$iconBasePath/analytics_icon.png";
  static const String cameraIcon = "$iconBasePath/camera_icon.png";
  static const String searchIcon = "$iconBasePath/search_icon.png";
  static const String filterIcon = "$iconBasePath/filter_icon.png";
  static const String notificationIcon = "$iconBasePath/notification_icon.png";
  static const String foodIcon = "$iconBasePath/food_icon.png";

  //Profile Screen Icons
  static const String personOutlinedIcon =
      "$iconBasePath/person_outlined_icon.png";
  static const String mailIcon = "$iconBasePath/mail_icon.png";
  static const String privacyPolicyIcon =
      "$iconBasePath/privacy_policy_icon.png";
  static const String settingIcon = "$iconBasePath/setting_icon.png";
  static const String chartIcon = "$iconBasePath/chart_icon.png";
}
