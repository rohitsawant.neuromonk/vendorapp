
import 'package:flutter/material.dart';
import 'package:vendorapp/Utils/route_names.dart';
import 'package:vendorapp/main.dart';
import '../Screens/Bottom Bar/bottom_bar_screen.dart';
import '../Screens/Home/edit_offer_screen.dart';
import '../Screens/Home/splash_screen.dart';
import '../Screens/Notification/notification_screen.dart';
import '../Screens/Onboarding/business_info_screen.dart';
import '../Screens/Onboarding/create_account_screen.dart';
import '../Screens/Onboarding/onboarding_screen.dart';
import '../Screens/Onboarding/payment_screen.dart';
import '../Screens/Onboarding/success_screen.dart';
import '../Screens/Onboarding/welcome_screen.dart';
import '../Screens/Profile/about_us.dart';
import '../Screens/Profile/personal_data_screen.dart';
import '../Screens/Profile/privacy_policy.dart';

class AppRouter {
  static Route generateRoute(RouteSettings routeSettings) {
    return MaterialPageRoute(
      builder: (context) {
        var args = routeSettings.arguments;
        switch (routeSettings.name) {
           case RouteNames.SPLASG_SCREEN:
            return const ScreenSplash();
          case RouteNames.ONBOARDING_SCREEN:
            return const OnBoardingScreen();
          case RouteNames.PERSONAL_DATA_SCREEN:
            return const PersonalDataScreen();
          case RouteNames.SUCCESS_SCREEN:
            return SuccessScreen(
              isQRSuccess: (args ?? false) as bool,
            );
          case RouteNames.PAYMENT_SCREEN:
            return const PaymentScreen();
          case RouteNames.CREATE_ACCOUNT_SCREEN:
            return const CreateAccountScreen();
          case RouteNames.BUSINESS_INFO_SCREEN:
            return const BusinessInfoScreen();
          case RouteNames.BOTTOM_BAR_SCREEN:
            return const BottomBarScreen();
          case RouteNames.NOTIFICATION_SCREEN:
            return const NotificationScreen();
          case RouteNames.PRIVACY_POLICY_SCREEN:
            return const PrivacyPolicy();
          case RouteNames.ABOUT_US_SCREEN:
            return const AboutUs();
          case RouteNames.EDIT_OFFER_SCREEN:
            return EditOfferScreen(
                offerIndex: args != null ? (args as int) : null);
          default:
            return const WelcomeScreen();
        }
      },
    );
  }
}
